import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';

import 'package:wee/repositories/favorites.dart';
import 'package:wee/api/hackernews_api.dart';
import 'package:wee/models/story.dart';

import 'dart:async';

class ListPage extends StatefulWidget {
  final HackernewsApi stories = HackernewsApi();
  final Favorites favorites = Favorites();

  ListPage({super.key});

  @override
  State<ListPage> createState() => _ListPageState(stories);
}

class _ListPageState extends State<ListPage> {
  final HackernewsApi _stories;
  List<Story> _list = [];
  List<Story> _favoriteStories = [];
  bool _isLoading = true;

  final ScrollController _controller = ScrollController();

  _ListPageState(this._stories);

  @override
  void initState() {
    super.initState();
    setState(() {
      _isLoading = false;
    });
    _getStories();
    _controller.addListener(() {
      if (_controller.position.atEdge) {
        if (_controller.position.pixels == 0) {
          // You're at the top.
        } else {
          _loadNextPage();
        }
      }
    });
  }

  void _getStories() async {
    if (_stories.stories().isEmpty) {
      Timer.periodic(const Duration(milliseconds: 500), (timer) async {
        if (_stories.stories().isNotEmpty) {
          _list = _stories.stories();
          var favorites = await widget.favorites.fetchAll();
          setState(() {
            _favoriteStories = favorites;
            _isLoading = false;
          });
          timer.cancel();
        }
      });
    } else {
      var favorites = await widget.favorites.fetchAll();
      setState(() {
        _list = _stories.stories();
        _favoriteStories = favorites;
        _isLoading = false;
      });
    }
  }

  void _loadNextPage() async {
    if (_isLoading) return;

    setState(() {
      _isLoading = true;
    });

    var nextPage = await _stories.loadNextPage();

    setState(() {
      _list += nextPage;
      _isLoading = false;
    });
  }

  Icon favIcon(Story story) {
    return _favoriteStories.any((fav) => fav.externalId == story.externalId)
        ? const Icon(Icons.favorite, color: Colors.redAccent)
        : const Icon(Icons.favorite_border);
  }

  void toggleFav(Story story) async {
    _favoriteStories.any((e) => e.externalId == story.externalId)
        ? await widget.favorites.remove(story)
        : await widget.favorites.add(story);

    var result = await widget.favorites.fetchAll();

    setState(() {
      _favoriteStories = result;
    });
  }

  _launchUrl(String url) async {
    final Uri uri = Uri.parse(url);
    if (!await launchUrl(uri)) {
      throw Exception('Could not launch $uri');
    }
  }

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      controller: _controller,
      shrinkWrap: true,
      itemCount: _list.length + 1,
      itemBuilder: (context, index) {
        // TODO: a hack basically
        if (index == _list.length) {
          return _loader();
        }

        return _buildItem(index);
      },
    );
  }

  Card _buildItem(int index) {
    return Card(
        child: ListTile(
      title: Text(_list[index].title),
      subtitle: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          const Icon(Icons.arrow_upward),
          Text(_list[index].score.toString())
        ],
      ),
      trailing: IconButton(
        icon: favIcon(_list[index]),
        onPressed: () => toggleFav(_list[index]),
      ),
      onTap: () => _launchUrl(_list[index].url),
    ));
  }

  Center _loader() {
    return Center(
      child: Container(
        width: 50.0,
        height: 50.0,
        padding: const EdgeInsets.all(10.0),
        child: const CircularProgressIndicator(),
      ),
    );
  }
}
