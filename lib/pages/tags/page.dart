import 'package:flutter/material.dart';
import 'package:wee/models/tag.dart';
import 'package:wee/pages/tags/form.dart';
import 'package:wee/repositories/tags.dart';

enum ButtonActions { edit, delete }

class TagsPage extends StatefulWidget {
  final Tags tagsRepository = Tags();

  TagsPage({super.key});

  @override
  State<TagsPage> createState() => _TagsPageState();
}

class _TagsPageState extends State<TagsPage> {
  List<Tag> _list = [];
  PersistentBottomSheetController? _bottomSheetController;

  _TagsPageState();

  @override
  void initState() {
    super.initState();
    _getTags();
  }

  void _getTags() async {
    var list = await widget.tagsRepository.fetchAll();

    setState(() {
      _list = list;
    });
  }

  void _deleteSource(Tag tag) async {
    await widget.tagsRepository.remove(tag);

    _getTags();
  }

  void _showForm(BuildContext context, Tag? tag) {
    _bottomSheetController =
        Scaffold.of(context).showBottomSheet<void>((BuildContext context) {
      return SizedBox(
        height: 300,
        child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Padding(
                  padding: const EdgeInsets.all(10.0),
                  child: TagForm(
                    params: TagParams(tag?.id, tag?.name ?? ""),
                    onSave: () => setState(() {
                      _hideBottomSheet();
                      _getTags();
                    }),
                  ))
            ],
          ),
        ),
      );
    });
  }

  void _hideBottomSheet() {
    _bottomSheetController?.close();
  }

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      shrinkWrap: true,
      itemCount: _list.length + 1,
      itemBuilder: (context, index) {
        if (index == _list.length) {
          return IconButton(
              icon: const Icon(Icons.add),
              color: Colors.green,
              onPressed: () => {_showForm(context, null)});
        }

        return buildItem(index);
      },
    );
  }

  Card buildItem(int index) {
    return Card(
        child: ListTile(
      title: Text(_list[index].name),
      trailing: IconButton(
        icon: PopupMenuButton<ButtonActions>(
          itemBuilder: (context) => <PopupMenuEntry<ButtonActions>>[
            PopupMenuItem<ButtonActions>(
              value: ButtonActions.edit,
              child: const Text('Edit'),
              onTap: () => {_showForm(context, _list[index])},
            ),
            PopupMenuItem<ButtonActions>(
              value: ButtonActions.delete,
              child: const Text('Delete'),
              onTap: () => {_deleteSource(_list[index])},
            ),
          ],
        ),
        onPressed: () => {},
      ),
      onTap: () => {},
    ));
  }
}
