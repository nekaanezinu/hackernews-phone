import 'package:flutter/material.dart';
import 'package:wee/models/tag.dart';
import 'package:wee/repositories/tags.dart';

class TagParams {
  int? id;
  String name;

  TagParams(this.id, this.name);
}

class TagForm extends StatefulWidget {
  final TagParams params;
  final Tags repository = Tags();
  final Function onSave;

  TagForm({super.key, required this.params, required this.onSave});

  @override
  TagFormState createState() {
    return TagFormState();
  }
}

class TagFormState extends State<TagForm> {
  final _formKey = GlobalKey<FormState>();
  List<Tag>? _tags;

  Future<List<Tag>> get tags async {
    List<Tag> tmpTags = [];

    if (_tags == null) {
      tmpTags = await widget.repository.fetchAll();
    }

    return tmpTags;
  }

  late TextEditingController _nameController;

  @override
  void initState() {
    super.initState();
    _nameController = TextEditingController(text: widget.params.name);
  }

  @override
  Widget build(BuildContext context) {
    return Form(
      key: _formKey,
      child: Column(
        children: [
          TextFormField(
            controller: _nameController,
            // initialValue: widget.params.name,
            decoration: const InputDecoration(labelText: "Name"),
            validator: (value) {
              if (value == null || value.isEmpty) {
                return 'Please enter a name';
              }
              if (widget.params.id == null) {
                return 'A source with this name already exists';
              }
              return null;
            },
          ),
          Padding(
              padding: const EdgeInsets.only(top: 25.0),
              child: ElevatedButton(
                onPressed: save,
                style: ButtonStyle(
                  backgroundColor: MaterialStateProperty.resolveWith<Color?>(
                    (Set<MaterialState> states) {
                      return const Color.fromARGB(255, 60, 139, 63);
                    },
                  ),
                ),
                child: const Text("Save"),
              ))
        ],
      ),
    );
  }

  void save() async {
    if (_formKey.currentState!.validate()) {
      try {
        if (widget.params.id == null) {
          await widget.repository
              .add(Tag(id: null, name: _nameController.text));
        } else {
          await widget.repository
              .update(Tag(id: widget.params.id, name: _nameController.text));
        }
      } catch (e) {
        rethrow;
      } finally {
        widget.onSave();
      }
    }
  }

  @override
  void dispose() {
    _nameController.dispose();
    super.dispose();
  }
}
