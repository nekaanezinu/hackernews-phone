import 'package:flutter/material.dart';
import 'package:wee/models/source.dart';
import 'package:wee/repositories/sources.dart';
import 'package:wee/pages/sources/form.dart';

enum ButtonActions { edit, delete }

class SourcesPage extends StatefulWidget {
  final Sources sourcesRepository = Sources();

  SourcesPage({super.key});

  @override
  State<SourcesPage> createState() => _SourcesPageState();
}

class _SourcesPageState extends State<SourcesPage> {
  List<Source> _list = [];
  PersistentBottomSheetController? _bottomSheetController;

  _SourcesPageState();

  @override
  void initState() {
    super.initState();
    _getSources();
  }

  void _getSources() async {
    var list = widget.sourcesRepository.sources;

    setState(() {
      _list = list;
    });
  }

  void _deleteSource(Source source) async {
    await widget.sourcesRepository.remove(source);

    _getSources();
  }

  void _showForm(BuildContext context, Source? source) {
    _bottomSheetController =
        Scaffold.of(context).showBottomSheet<void>((BuildContext context) {
      return SizedBox(
        height: 300,
        child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Padding(
                  padding: const EdgeInsets.all(10.0),
                  child: SourceForm(
                    params: SourceParams(
                        source?.id, source?.name ?? "", source?.url ?? ""),
                    repository: widget.sourcesRepository,
                    onSave: () => setState(() {
                      _hideBottomSheet();
                      _getSources();
                    }),
                  ))
            ],
          ),
        ),
      );
    });
  }

  void _hideBottomSheet() {
    _bottomSheetController?.close();
  }

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      shrinkWrap: true,
      itemCount: _list.length + 1,
      itemBuilder: (context, index) {
        if (index == _list.length) {
          return IconButton(
              icon: const Icon(Icons.add),
              color: Colors.green,
              onPressed: () => {_showForm(context, null)});
        }

        return buildItem(index);
      },
    );
  }

  Card buildItem(int index) {
    return Card(
        child: ListTile(
      title: Text(_list[index].name),
      subtitle: Text(_list[index].url),
      trailing: IconButton(
        icon: PopupMenuButton<ButtonActions>(
          itemBuilder: (context) => <PopupMenuEntry<ButtonActions>>[
            PopupMenuItem<ButtonActions>(
              value: ButtonActions.edit,
              child: const Text('Edit'),
              onTap: () => {_showForm(context, _list[index])},
            ),
            PopupMenuItem<ButtonActions>(
              value: ButtonActions.delete,
              child: const Text('Delete'),
              onTap: () => {_deleteSource(_list[index])},
            ),
          ],
        ),
        onPressed: () => {},
      ),
      onTap: () => {},
    ));
  }
}
