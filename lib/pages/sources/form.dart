import 'package:flutter/material.dart';
import 'package:wee/models/source.dart';
import 'package:wee/repositories/sources.dart';

class SourceParams {
  int? id;
  String name;
  String url;

  SourceParams(this.id, this.name, this.url);
}

class SourceForm extends StatefulWidget {
  final SourceParams params;
  final Sources repository;
  final Function onSave;

  const SourceForm(
      {super.key,
      required this.params,
      required this.repository,
      required this.onSave});

  @override
  SourceFormState createState() {
    return SourceFormState();
  }
}

class SourceFormState extends State<SourceForm> {
  final _formKey = GlobalKey<FormState>();

  late TextEditingController _nameController;
  late TextEditingController _urlController;

  @override
  void initState() {
    super.initState();
    _nameController = TextEditingController(text: widget.params.name);
    _urlController = TextEditingController(text: widget.params.url);
  }

  @override
  Widget build(BuildContext context) {
    return Form(
      key: _formKey,
      child: Column(
        children: [
          TextFormField(
            controller: _nameController,
            // initialValue: widget.params.name,
            decoration: const InputDecoration(labelText: "Name"),
            validator: (value) {
              if (value == null || value.isEmpty) {
                return 'Please enter a name';
              }
              if (widget.params.id == null &&
                  widget.repository.sources.any((s) => s.name == value)) {
                return 'A source with this name already exists';
              }
              return null;
            },
          ),
          TextFormField(
            controller: _urlController,
            // initialValue: widget.params.url,
            decoration: const InputDecoration(labelText: "Url"),
            validator: (value) {
              if (value == null || value.isEmpty) {
                return 'Please enter a valid url';
              }
              if (widget.params.id == null &&
                  widget.repository.sources.any((s) => s.url == value)) {
                return 'A source with this url already exists';
              }
              return null;
            },
          ),
          Padding(
              padding: const EdgeInsets.only(top: 25.0),
              child: ElevatedButton(
                onPressed: save,
                style: ButtonStyle(
                  backgroundColor: MaterialStateProperty.resolveWith<Color?>(
                    (Set<MaterialState> states) {
                      return const Color.fromARGB(255, 60, 139, 63);
                    },
                  ),
                ),
                child: const Text("Save"),
              ))
        ],
      ),
    );
  }

  void save() async {
    if (_formKey.currentState!.validate()) {
      try {
        if (widget.params.id == null) {
          await widget.repository.add(Source(
              id: null, name: _nameController.text, url: _urlController.text));
        } else {
          await widget.repository.update(Source(
              id: widget.params.id,
              name: _nameController.text,
              url: _urlController.text));
        }
      } catch (e) {
        rethrow;
      } finally {
        widget.onSave();
      }
    }
  }

  @override
  void dispose() {
    _nameController.dispose();
    _urlController.dispose();
    super.dispose();
  }
}
