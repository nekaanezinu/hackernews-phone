import 'package:flutter/material.dart';

class SearchForm extends StatefulWidget {
  final Function onChanged;

  const SearchForm({super.key, required this.onChanged});

  @override
  SearchFormState createState() {
    return SearchFormState();
  }
}

class SearchFormState extends State<SearchForm> {
  String _search = '';

  void _changed(String value) async {
    setState(() => _search = value);
    widget.onChanged(_search);
  }

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      decoration: const InputDecoration(labelText: "Search"),
      onChanged: _changed,
    );
  }
}
