import 'package:flutter/material.dart';
import 'package:wee/models/tag.dart';
import 'package:wee/repositories/sources.dart';
import 'package:wee/repositories/tags.dart';

enum ComponentState { active, inactive }

enum SearchState { home, loading, results }

class TagForm extends StatefulWidget {
  final Sources sourcesRepository;
  final Tags tagsRepository;
  final Function onSave;
  final TagSearch search;

  TagForm(this.sourcesRepository, this.tagsRepository, this.onSave, {super.key})
      : search = TagSearch();

  @override
  TagFormState createState() {
    return TagFormState();
  }
}

class TagFormState extends State<TagForm> {
  final _formKey = GlobalKey<FormState>();
  ComponentState _state = ComponentState.inactive;
  List<Tag> _tags = [];

  late TextEditingController _searchController;

  @override
  void initState() {
    super.initState();
    _searchController = TextEditingController(text: '');
    _searchController.addListener(_onSearchChanged);
  }

  void _onSearchChanged() async {
    String value = _searchController.text;

    if (value.length >= 3) {
      widget.search
        ..setQuery(value)
        ..setState(SearchState.loading);

      var result = await widget.search.search();

      setState(() {
        _tags = result;
      });
    }
  }

  void save() async {
    try {} catch (e) {
      rethrow;
    } finally {
      widget.onSave();
    }
  }

  @override
  Widget build(BuildContext context) {
    return Form(
      key: _formKey,
      child: Column(
        children: [
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 8, vertical: 16),
            child: TextFormField(
              controller: _searchController,
              decoration: const InputDecoration(
                border: OutlineInputBorder(),
                hintText: 'Search',
              ),
            ),
          ),
        ],
      ),
    );
  }

  @override
  void dispose() {
    _searchController.dispose();
    super.dispose();
  }
}

class TagSearch {
  SearchState _state = SearchState.home;
  String _query = '';
  final Tags _tags = Tags();

  TagSearch();

  SearchState state() => _state;

  void setQuery(String query) {
    _query = query;
  }

  void setState(SearchState state) {
    _state = state;
  }

  Future<List<Tag>> search() async {
    return _tags.search(_query);
  }
}
