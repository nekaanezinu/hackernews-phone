import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:wee/pages/favs/search.dart';

import 'package:wee/repositories/favorites.dart';
import 'package:wee/models/story.dart';
import 'package:wee/repositories/tags.dart';

enum ButtonActions { tag, delete }

class FavPage extends StatefulWidget {
  final Favorites favorites = Favorites();
  final Tags tags = Tags();

  FavPage({super.key});

  @override
  State<FavPage> createState() => _FavPageState();
}

class _FavPageState extends State<FavPage> {
  var _stories = <Story>[]; // idk if i like this
  String _seachQuery = '';

  _FavPageState();

  @override
  void initState() {
    super.initState();
    _getFavs();
  }

  void _getFavs() async {
    var favs = await widget.favorites.fetchAll();
    setState(() {
      _stories = favs;
    });
  }

  _launchUrl(String url) async {
    final Uri uri = Uri.parse(url);
    if (!await launchUrl(uri)) {
      throw Exception('Could not launch $uri');
    }
  }

  void _deleteFav(Story story) async {
    await widget.favorites.remove(story);

    var result = await widget.favorites.fetchAll();

    setState(() {
      _stories = result;
    });
  }

  void _showTags(Story story) async {}

  void _search(String query) async {
    _seachQuery = query;
    var tmpStories = await widget.favorites.search(query);
    setState(() {
      _stories = tmpStories;
    });
  }

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      shrinkWrap: true,
      itemCount: _stories.length + 1,
      itemBuilder: (context, index) {
        var offsetIndex = index - 1;

        if (offsetIndex < 0) {
          return SearchForm(
            onChanged: _search,
          );
        }

        return Card(
            child: ListTile(
          title: Text(_stories[offsetIndex].title),
          subtitle: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              const Icon(Icons.arrow_upward),
              Text(_stories[offsetIndex].score.toString())
            ],
          ),
          trailing: _menu(offsetIndex),
          onTap: () => _launchUrl(_stories[offsetIndex].url),
        ));
      },
    );
  }

  IconButton _menu(int index) {
    return IconButton(
      icon: PopupMenuButton<ButtonActions>(
        itemBuilder: (context) => <PopupMenuEntry<ButtonActions>>[
          PopupMenuItem<ButtonActions>(
            value: ButtonActions.tag,
            child: const Text('Tag'),
            onTap: () => {_showTags(_stories[index])},
          ),
          PopupMenuItem<ButtonActions>(
            value: ButtonActions.delete,
            child: const Text('Delete'),
            onTap: () => {_deleteFav(_stories[index])},
          ),
        ],
      ),
      onPressed: () => {},
    );
  }
}
