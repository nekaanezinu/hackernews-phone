import 'package:sqflite/sqflite.dart';

Future onConfigure(Database db) async {
  await db.execute('PRAGMA foreign_keys = ON');
}

void _createTableStoriesV2(Batch batch) {
  batch.execute('DROP TABLE IF EXISTS stories');
  batch.execute('''
    CREATE TABLE stories (
      id INTEGER PRIMARY KEY AUTOINCREMENT,
      score INTEGER NOT NULL DEFAULT 0,
      title TEXT NOT NULL,
      url TEXT NOT NULL,
      time INTEGER NOT NULL DEFAULT 0,
      favorited_at INTEGER NOT NULL DEFAULT 0,
      source_id INTEGER NOT NULL,
      FOREIGN KEY (source_id) REFERENCES sources(id),
      external_id TEXT
    )
  ''');
}

void _updateTableStoriesV1toV2(Batch batch) {
  batch.execute('ALTER TABLE stories ADD external_id TEXT');
}

void migrate20230925124436(String path) async {
  await openDatabase(path,
      version: 2,
      onConfigure: onConfigure,
      onCreate: _onCreate,
      onUpgrade: _onUpgrade,
      onDowngrade: onDatabaseDowngradeDelete,
      singleInstance: false);
}

void _onCreate(Database db, int version) async {
  var batch = db.batch();
  _createTableStoriesV2(batch);
  await batch.commit();
}

void _onUpgrade(Database db, int oldVersion, int newVersion) async {
  var batch = db.batch();
  if (oldVersion == 1) {
    _updateTableStoriesV1toV2(batch);
  }
  await batch.commit();
}
