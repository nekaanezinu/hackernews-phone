import 'package:sqflite/sqflite.dart';

Future onConfigure(Database db) async {
  await db.execute('PRAGMA foreign_keys = ON');
}

void _createTableSourcesV1(Batch batch) {
  batch.execute('DROP TABLE IF EXISTS sources');
  batch.execute('''
    CREATE TABLE IF NOT EXISTS sources (
      id INTEGER PRIMARY KEY AUTOINCREMENT,
      name TEXT NOT NULL,
      url TEXT NOT NULL
    )
  ''');
}

void _createTableTagsV1(Batch batch) {
  batch.execute('DROP TABLE IF EXISTS tags');
  batch.execute('''
    CREATE TABLE IF NOT EXISTS tags (
      id INTEGER PRIMARY KEY AUTOINCREMENT,
      name TEXT NOT NULL
    )
  ''');
}

void _createTableStoriesV1(Batch batch) {
  batch.execute('DROP TABLE IF EXISTS stories');
  batch.execute('''
    CREATE TABLE stories (
      id INTEGER PRIMARY KEY AUTOINCREMENT,
      score INTEGER NOT NULL DEFAULT 0,
      title TEXT NOT NULL,
      url TEXT NOT NULL,
      time INTEGER NOT NULL DEFAULT 0,
      favorited_at INTEGER NOT NULL DEFAULT 0,
      source_id INTEGER NOT NULL,
      FOREIGN KEY (source_id) REFERENCES sources(id)
    )
  ''');
}

void _createTableStoryTagsV1(Batch batch) {
  batch.execute('DROP TABLE IF EXISTS story_tags');
  batch.execute('''
    CREATE TABLE story_tags (
      id INTEGER PRIMARY KEY AUTOINCREMENT,
      story_id INTEGER NOT NULL,
      tag_id INTEGER NOT NULL,
      FOREIGN KEY (story_id) REFERENCES stories(id),
      FOREIGN KEY (tag_id) REFERENCES tags(id)
    )
  ''');
}

void migrate20230925124435(String path) async {
  await openDatabase(path, version: 1, onCreate: _onCreate, onDowngrade: onDatabaseDowngradeDelete, singleInstance: false);
}

void _onCreate(Database db, int version) async {
  var batch = db.batch();
  _createTableSourcesV1(batch);
  _createTableStoriesV1(batch);
  _createTableTagsV1(batch);
  _createTableStoryTagsV1(batch);
  await batch.commit();
}
