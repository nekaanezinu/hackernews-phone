import 'package:sqflite/sqflite.dart';
import 'migrations/20230925124435.dart';
import 'migrations/20230925124436.dart';

// migrations https://github.com/tekartik/sqflite/blob/master/sqflite/doc/migration_example.md

class DB {
  static final DB _instance = DB._internal();
  DB._internal() {
    _db = _fetchDb();
  }
  factory DB() => _instance;

  Future<Database>? _db;
  Future<Database> get db => _db!;

  Future<Database> _fetchDb() async {
    String databasesPath = await getDatabasesPath();
    String path = '$databasesPath/newsapp.db';
    var database = await openDatabase(path);

    _migrate(path, await database.getVersion());
    _addFirstSource(database);
    return database;
  }

  void _migrate(String path, int version) {
    switch (version) {
      case 0:
        {
          migrate20230925124435(path); // 1
        }
        break;

      case 1:
        {
          migrate20230925124436(path); // 2
        }
        break;
    }
  }

  _addFirstSource(Database database) async {
    var data = await database.query('sources');
    if (data.isEmpty) {
      await database.insert('sources', {
        'id': 1,
        'name': 'Hackernews',
        'url': 'https://news.ycombinator.com/'
      });
    }
  }
}
