import 'package:wee/models/tag.dart';
import 'package:wee/repositories/tags.dart';

class Story {
  int? id;
  int score;
  DateTime time;
  String title;
  String url;
  DateTime? favoritedAt;
  int sourceId;
  String? externalId;
  List<Tag>? _tags;

  Story(
      {required this.id,
      required this.score,
      required this.time,
      required this.title,
      required this.url,
      this.favoritedAt,
      required this.sourceId,
      required this.externalId});

  factory Story.fromDb(Map<String, dynamic> map) {
    return Story(
      id: map['id'],
      score: map['score'],
      time: DateTime.fromMillisecondsSinceEpoch(map['time']),
      title: map['title'],
      url: map['url'],
      sourceId: map['source_id'],
      externalId: map['external_id'],
      favoritedAt: map['favorited_at'] == null
          ? null
          : DateTime.fromMillisecondsSinceEpoch(map['favorited_at']),
    );
  }

  Map<String, dynamic> toDbMap() {
    return {
      'id': id,
      'score': score,
      'title': title,
      'time': time.millisecondsSinceEpoch,
      'url': url,
      'source_id': 1,
      'external_id': externalId,
      'favorited_at': favoritedAt?.millisecondsSinceEpoch,
    };
  }

  factory Story.fromHackernews(Map<String, dynamic> map) {
    return Story(
        id: null,
        score: map['score'] as int,
        time: DateTime.fromMillisecondsSinceEpoch(map['time'] * 1000 as int),
        title: map['title'] as String,
        url: map['url'] ?? "https://news.ycombinator.com/item?id=${map['id']}",
        sourceId: 1,
        externalId: map['id'].toString());
  }

  Future<List<Tag>> get tags async {
    if (_tags != null) return _tags!;

    _tags ??= await Tags().forStories([this]);

    return _tags!;
  }

  @override
  String toString() {
    return 'Story {id $id, title: $title}';
  }
}
