class Tag {
  int? id;
  String name;

  Tag({this.id, required this.name});

  factory Tag.fromDb(Map<String, dynamic> map) {
    return Tag(
      id: map['id'],
      name: map['name'],
    );
  }

  Map<String, dynamic> toDbMap() {
    return {
      'id': id,
      'name': name,
    };
  }

  @override
  String toString() {
    return 'Tag {id $id, title: $name}';
  }
}
