class Source {
  int? id;
  String name;
  String url;

  Source({this.id, required this.name, required this.url});

  factory Source.fromDb(Map<String, dynamic> map) {
    return Source(
      id: map['id'],
      name: map['name'],
      url: map['url'],
    );
  }

  Map<String, dynamic> toDbMap() {
    return {
      'id': id,
      'name': name,
      'url': url,
    };
  }

  @override
  String toString() {
    return 'Source {id $id, title: $name}';
  }
}
