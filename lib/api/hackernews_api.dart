import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:wee/models/story.dart';

class HackernewsApi {
  List<int> _storyIds = [];
  final List<Story> _list = [];
  static const int _pageSize = 15;

  HackernewsApi() {
    _getStories();
  }

  void _getStories() async {
    _storyIds = await _topStories();
    _storyIds.take(_pageSize).forEach((id) async {
      final Story story = await _getStory(id);
      _list.add(story);
    });
  }

  List<Story> stories() {
    return _list;
  }

  Future<List<Story>> loadNextPage() async {
    var cursor = _list.length;

    if (cursor >= _storyIds.length) {
      throw Exception('Max pages reached');
    }

    var nextPageIds = _storyIds.skip(cursor).take(cursor + _pageSize);

    for (var id in nextPageIds) {
      print("fetching story $id");
      final Story story = await _getStory(id);
      _list.add(story);
    }

    return _list.skip(cursor).toList();
  }
}

const baseUrl = 'https://hacker-news.firebaseio.com/v0/';

Future<List<int>> _topStories() async {
  // 500 top ranked story ids
  final url = Uri.parse('${baseUrl}topstories.json?print=pretty');
  final response = await http.get(url);
  final List<dynamic> data = json.decode(response.body);

  List<int> ids = data.map((e) {
    if (e is int) return e;
    int intValue = int.tryParse(e?.toString() ?? '') ??
        0; // Try to parse the element to int, default to 0 if it fails.
    return intValue;
  }).toList();

  return ids;
}

Future<Story> _getStory(int id) async {
  final url = Uri.parse('${baseUrl}item/$id.json?print=pretty');
  final response = await http.get(url);
  final Map<String, dynamic> data = json.decode(response.body);

  return Story.fromHackernews(data);
}
