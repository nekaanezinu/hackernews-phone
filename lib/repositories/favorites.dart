import 'package:wee/models/story.dart';
import 'package:wee/db/db.dart';

class Favorites {
  Future<List<Story>> fetchAll() async {
    var favorites = await DB()
        .db
        .then((db) => db.query('stories', orderBy: 'favorited_at DESC'));
    return List.generate(favorites.length, (i) => Story.fromDb(favorites[i]));
  }

  Future<void> add(Story story) async {
    story.favoritedAt = DateTime.now();
    await DB().db.then((db) => db.insert('stories', story.toDbMap()));
  }

  Future<void> remove(Story story) async {
    await DB().db.then((db) => db.delete('stories',
        where: 'external_id = ?', whereArgs: [story.externalId]));
  }

  Future<List<Story>> search(String query) async {
    var favorites = await DB().db.then((db) =>
        db.query('stories', where: 'title LIKE?', whereArgs: ['%$query%']));

    return List.generate(favorites.length, (i) => Story.fromDb(favorites[i]));
  }
}
