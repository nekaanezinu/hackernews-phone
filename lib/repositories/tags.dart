import 'package:wee/db/db.dart';
import 'package:wee/models/story.dart';
import 'package:wee/models/tag.dart';

class Tags {
  Future<List<Tag>> fetchAll() async {
    var favStories = await DB().db.then((db) => db.query('tags'));
    return List.generate(favStories.length, (i) => Tag.fromDb(favStories[i]));
  }

  Future<void> add(Tag tag) async {
    await DB().db.then((db) => db.insert('tags', tag.toDbMap()));
  }

  Future<void> update(Tag tag) async {
    await DB().db.then((db) =>
        db.update('tags', tag.toDbMap(), where: 'id = ?', whereArgs: [tag.id]));
  }

  Future<void> remove(Tag tag) async {
    await DB()
        .db
        .then((db) => db.delete('tags', where: 'id = ?', whereArgs: [tag.id]));
  }

  Future<List<Tag>> search(String query) async {
    var result = await DB().db.then(
        (db) => db.query('tags', where: 'name LIKE?', whereArgs: ['%$query%']));

    return result.map((e) => Tag.fromDb(e)).toList();
  }

  Future<List<Tag>> forStories(List<Story> stories) async {
    var result = await DB().db.then(
          (db) => db.query(
            'tags',
            where:
                'id IN (SELECT tag_id FROM stories_tags WHERE story_id IN (?))',
            whereArgs: [stories.map((e) => e.id).toList()],
          ),
        );

    return result.map((e) => Tag.fromDb(e)).toList();
  }
}
