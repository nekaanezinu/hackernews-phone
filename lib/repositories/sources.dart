import 'package:wee/models/source.dart';
import 'package:wee/db/db.dart';

class Sources {
  List<Source> sources = [];

  Sources() {
    _load();
  }

  bool contains(Source source) {
    return sources.any((element) => element.name == source.name);
  }

  Future<void> _load() async {
    var favStories = await DB().db.then((db) => db.query('sources'));
    sources =
        List.generate(favStories.length, (i) => Source.fromDb(favStories[i]));
  }

  Future<void> add(Source source) async {
    if (contains(source)) {
      return;
    }

    await DB().db.then((db) => db.insert('sources', source.toDbMap()));

    await _load();
  }

  Future<void> update(Source source) async {
    await DB().db.then((db) => db.update('sources', source.toDbMap(),
        where: 'id = ?', whereArgs: [source.id]));

    await _load();
  }

  Future<void> remove(Source source) async {
    if (!contains(source)) {
      return;
    }

    await DB().db.then(
        (db) => db.delete('sources', where: 'id = ?', whereArgs: [source.id]));

    await _load();
  }
}
