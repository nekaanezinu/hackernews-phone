import 'package:flutter/material.dart';
import 'package:wee/pages/tags/page.dart';
import 'package:wee/repositories/sources.dart';
import 'package:wee/repositories/tags.dart';
import 'repositories/favorites.dart';
import 'package:wee/pages/list/page.dart';
import 'package:wee/pages/favs/page.dart';
import 'package:wee/pages/sources/page.dart';
import 'package:wee/api/hackernews_api.dart';

class Home extends StatefulWidget {
  Home({Key? key}) : super(key: key);
  final Favorites favorites = Favorites();
  final HackernewsApi stories = HackernewsApi();
  final Sources sources = Sources();
  final Tags tags = Tags();

  @override
  HomeState createState() => HomeState();
}

class HomeState extends State<Home> {
  int _selectedIndex = 0;
  List<Widget> _widgetOptions = [];

  HomeState() {
    _widgetOptions = [
      ListPage(),
      FavPage(),
      TagsPage(),
      SourcesPage(),
    ];
  }

  void _onNavTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Theme.of(context).colorScheme.inversePrimary,
        // title: Text(widget.title),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Expanded(child: _widgetOptions.elementAt(_selectedIndex))
          ],
        ),
      ),
      bottomNavigationBar: BottomNavigationBar(
        items: const <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: Icon(Icons.newspaper),
            label: 'News',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.favorite),
            label: 'Favorites',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.label),
            label: 'Tags',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.rss_feed),
            label: 'Sources',
          ),
        ],
        currentIndex: _selectedIndex,
        selectedItemColor: Colors.deepPurple[300],
        onTap: _onNavTapped,
        type: BottomNavigationBarType.fixed,
      ),
    );
  }
}
